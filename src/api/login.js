import main from "@/components/common/main";
import { useCookies} from "vue3-cookies";

export function getInfoSupplier() {
  const {cookies} = useCookies();
  return main({
    url: '/supplier-app/getAccountSupplier',
    method: 'get',
    headers: {
      'Authorization': 'Bearer ' + cookies.get('token')
    }
  })
}
export function login(username, password){
  return main({
    data:{
      username: username,
      password: password,
      role: "supplier" ,//warehouse, supplier, driver,
      device_token:"supplier"
    },
    url: '/supplier-app/login',
    method: 'post'
  })
}
export function getIdSupplier(id){
  return main({
    url: '/getInfoSupplier/' + id,
    method:'get'
  })
}