import main from "@/components/common/main";
import { useCookies} from "vue3-cookies";


export function getInfo() {
  return main({
    url: '/info-user',
    method: 'get'
  })
}
export function uploadFile(file){
  const {cookies} = useCookies();
  return main({
    data: file,
    url: '/supplier-app/importFileExcelPrice',
    method: 'post',
    headers: {
      'Content-Type': 'miltipart/formdata',
      'Authorization': 'Bearer ' + cookies.get('token')
    }
  })
}

export function uploadFileById(file){
  console.log(file)
  return main({
    data: file,
    url: '/importFile',
    method: 'post',
    headers: {
      'Content-Type': 'miltipart/formdata',
    }
  })
}