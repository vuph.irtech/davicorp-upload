// import Vue from 'vue'
// import VueAxios from "vue-axios";
import axios from "axios";

// const API_URL = "http://localhost:8000/api";
const API_URL = process.env.VUE_APP_BASE_API;

export default axios.create({
  baseURL: API_URL,
});