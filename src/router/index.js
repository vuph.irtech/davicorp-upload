import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'
import UploadFileView from '@/views/UploadFileView'
import { useCookies } from 'vue3-cookies'
import NotFoundVue from '@/components/NotFound.vue'

const {cookies} = useCookies();

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/login',
    name: "login",
    component: LoginView
  },
  {
    path: '/import-file/:id',
    name: 'import',
    component: UploadFileView
  },
  {
    path: '/:pathMatch(.*)*',
    component: NotFoundVue
  },
]
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})
router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!(cookies.get('token'))) {
      router.push({ path: '/login', name: 'login', component: LoginView })
    }
  } else {
    next()
  }
})

export default router
