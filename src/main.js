import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import { loadFonts } from './plugins/webfontloader'
import '@/assets/style/global.css'
import axios from 'axios'
import store from './store'
import VueSweetalert2 from 'vue-sweetalert2';


window.axios = axios

const options = {
  confirmButtonColor: '#41b882',
  cancelButtonColor: '#ff7674',
};


loadFonts()

createApp(App)
  .use(router)
  .use(vuetify)
  .use(store)
  .use(VueSweetalert2, options)
  .mount('#app')
